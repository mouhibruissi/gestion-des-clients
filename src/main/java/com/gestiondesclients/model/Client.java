package com.gestiondesclients.model;

import lombok.*;
import javax.persistence.Id;
import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Client implements Serializable {
    @Id
    private Long id;
    private String nom;
    private Integer cin;
    private Integer tel;
}
