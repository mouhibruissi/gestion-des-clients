package com.gestiondesclients.manager;

import com.gestiondesclients.model.Client;
import com.gestiondesclients.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;
import java.util.Optional;

@Service
public class ClientManager {
    @Autowired
    private ClientRepository clientRepository;



    @GetMapping()
    public List<Client> getClients(){
        return clientRepository.findAll();
    }

    @PutMapping
    public void addNewClient(Client client) {
        clientRepository.save(client);
    }
    @DeleteMapping
    public void deleteClient(Long clientId) {
    clientRepository.deleteById(clientId);
    }
}
